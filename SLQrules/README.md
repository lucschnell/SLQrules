# Complete Set of Feynman Rules for Scalar Leptoquarks
### Luc Schnell, October 2021
The **FeynRules model files** are located in this folder. They are based on existing model files for the Standard Model and the individual scalar leptoquarks [[1](https://arxiv.org/abs/1310.1921), [3](https://arxiv.org/abs/1801.07641)]. The scalar leptoquark fields defined in [[3](https://arxiv.org/abs/1801.07641)] were combined in the model file [SLQrules.fr](SLQrules.fr) and their interactions with the **SM gauge fields** and **fermions**, as well as the **LQ-LQ-Higgs(-Higgs), LQ-LQ-LQ(-Higgs) and LQ-LQ-LQ-LQ interactions** were added.


### SLQrules.fr
The file [SLQrules.fr](SLQrules.fr) is the main FeynRules model file. It consists of three parts: the first part contains the **parameter definitions**, the second part the **definition of the leptoquark fields** and the third part the **leptoquark Lagrangians**.

#### Parameter definitions
The following table contains information about the parameters defined in [SLQrules.fr](SLQrules.fr).

| Parameter: Latex name | Parameter: Code name | Description |
| ------ | ------ | ------ |
| $`m_1`$ | `m1` | Mass of $`\Phi_1`$ leptoquark. |
| $`Y_1^{LL}`$ | `Y1LL` | Coupling of $`\Phi_1`$ leptoquark to a left-handed quark and a left-handed lepton. |
| $`Y_1^{RR}`$ | `Y1RR` | Coupling of $`\Phi_1`$ leptoquark to a right-handed quark and a right-handed lepton. |
| $`Y_1^{Q,LL}`$ | `Y1QLL` | Coupling of $`\Phi_1`$ leptoquark to two left-handed quarks. |
| $`Y_1^{Q,RR}`$ | `Y1QRR` | Coupling of $`\Phi_1`$ leptoquark to two right-handed quarks. |
| $`m_{\tilde{1}}`$ | `m1t` | Mass of $`\Phi_{\tilde{1}}`$ leptoquark. |
| $`Y_{\tilde{1}}^{RR}`$ | `Y1tRR` | Coupling of $`\Phi_{\tilde{1}}`$ leptoquark to a right-handed quark and a right-handed fermion. |
| $`Y_{\tilde{1}}^{Q, RR}`$ | `Y1tQRR` | Coupling of $`\Phi_{\tilde{1}}`$ leptoquark to two right-handed quarks. |
| $`m_2`$ | `m2` | Mass of $`\Phi_2`$ leptoquark. |
| $`Y_2^{RL}`$ | `Y2RL` | Coupling of $`\Phi_2`$ leptoquark to a right-handed quark and a left-handed fermion. |
| $`Y_2^{LR}`$ | `Y2LR` | Coupling of $`\Phi_2`$ leptoquark to a left-handed quark and a right-handed fermion. |
| $`m_{\tilde{2}}`$ | `m2t` | Mass of $`\Phi_{\tilde{2}}`$ leptoquark. |
| $`Y_{\tilde{2}}^{RL}`$ | `Y2tRL` | Coupling of $`\Phi_{\tilde{2}}`$ leptoquark to a right-handed quark and left-handed fermion. |
| $`m_3`$ | `m3` | Mass of $`\Phi_3`$ leptoquark. |
| $`Y_3^{LL}`$ | `Y3LL` | Coupling of $`\Phi_3`$ leptoquark to a left-handed quark and a left-handed fermion. |
| $`Y_3^{Q, LL}`$ | `Y3QLL` | Coupling of $`\Phi_3`$ leptoquark to two left-handed quarks. |
| $`W^{-1/3}`$ | `W13mat` | Mass mixing diagonalization matrix for $`Q = -\frac{1}{3}`$. |
| $`W^{+2/3}`$ | `W23mat` | Mass mixing diagonalization matrix for $`Q = +\frac{2}{3}`$. |
| $`W^{-4/3}`$ | `W43mat` | Mass mixing diagonalization matrix for $`Q = -\frac{4}{3}`$. |
| $`\hat{M}^{-1/3}_{11}`$,..., $`\hat{M}^{-4/3}_{22}`$, $`\hat{M}^{+5/3}`$| `m1m13hat`,...,`m3m43hat`,`m2p53hat` | Masses of the individual leptoquark components $`\hat{\Phi}^{q}_a`$ in the hat basis. |
| $`\hat{\Gamma}^{-1/3}_{1}`$,..., $`\hat{\Gamma}^{-4/3}_{3}`$, $`\hat{\Gamma}^{+5/3}`$| `W1m13hat`,...,`W3m43hat`,`W2p53hat` | Widths of the individual leptoquark components $`\hat{\Phi}^{q}_a`$ in the hat basis. |
| $`A_{1\tilde{2}}`$,..., $`Y_{33}`$, $`Y_{1}`$,..., $`Y_{3}`$ | `A12t`,..., `Y33`, `Y1`,..., `Y3` | LQ-LQ-Higgs(-Higgs) interaction couplings. |
| $`A_{1\tilde{2}\tilde{2}}`$,..., $`Y_{\tilde{2}\tilde{2}\tilde{2}}`$ | `A12t2t`,..., `Y2t2t2t` | LQ-LQ-LQ(-Higgs) interaction couplings. |
| $`Y^{(1)}_{1}`$,..., $`Y^{(3)}_{1}`$,...,$`Y^{(5)}_{3}`$,...,$`Y'_{12\tilde{2}3}`$ | `Yo1`,...,`Yt1`,...,`Yf3`,..., `Y122t3prime` | LQ-LQ-LQ-LQ interaction couplings. |


#### Field definitions
The following table contains information about the fields that were defined in [SLQrules.fr](SLQrules.fr).

| Field: Latex name | Field: Code name | Description |
| ------ | ------ | ------ |
| $`\hat{\Phi}^{-1/3}_1`$ | `S1m13hat` | Singlet field $`\hat{\Phi}^{-1/3}_1`$ after mass diagonalization (physical). |
| $`\Phi^{-1/3}_1`$ | `S1m13` | Singlet field $`\Phi^{-1/3}_1`$ (unphysical). |
| $`\hat{\Phi}^{-4/3}_{\tilde{1}}`$ | `S1tm43hat` | Field $`\hat{\Phi}^{-4/3}_{\tilde{1}}`$ after mass diagonalization (physical). |
| $`\Phi^{-4/3}_{\tilde{1}}`$ | `S1tm43` | Singlet field $`\Phi^{-4/3}_{\tilde{1}}`$ (unphysical). |
| $`\hat{\Phi}^{+5/3}_{2}`$ | `R2p53hat` | Field $`\hat{\Phi}^{+5/3}_{2}`$ after mass diagonalization (physical). |
| $`\hat{\Phi}^{+2/3}_{2}`$ | `R2p23hat` | Field $`\hat{\Phi}^{+2/3}_{2}`$ after mass diagonalization (physical). |
| $`\Phi_{2}`$ | `R2` | Doublet field $`\Phi_{2}`$ (unphysical). |
| $`\hat{\Phi}^{-1/3}_{\tilde{2}}`$ | `R2tm13hat` | Field $`\hat{\Phi}^{-1/3}_{\tilde{2}}`$ after mass diagonalization (physical). |
| $`\hat{\Phi}^{+2/3}_{\tilde{2}}`$ | `R2tp23hat` | Field $`\hat{\Phi}^{+2/3}_{\tilde{2}}`$ after mass diagonalization (physical). |
| $`\Phi_{\tilde{2}}`$ | `R2t` | Doublet field $`\Phi_{\tilde{2}}`$ (unphysical). |
| $`\hat{\Phi}^{-1/3}_{3}`$ | `S3m13hat` | Field $`\hat{\Phi}^{-1/3}_{3}`$ after mass diagonalization (physical). |
| $`\hat{\Phi}^{+2/3}_{3}`$ | `S3p23hat` | Field $`\hat{\Phi}^{+2/3}_{3}`$ after mass diagonalization (physical). |
| $`\hat{\Phi}^{-4/3}_{3}`$ | `S3m43hat` | Field $`\hat{\Phi}^{-4/3}_{3}`$ after mass diagonalization (physical). |
| $`\Phi_{3}`$ | `S3` | Triplet field $`\Phi_{3}`$ (unphysical). |
