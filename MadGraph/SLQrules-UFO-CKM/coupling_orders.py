# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.2.0 for Mac OS X x86 (64-bit) (December 12, 2020)
# Date: Tue 30 Nov 2021 21:08:10


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

