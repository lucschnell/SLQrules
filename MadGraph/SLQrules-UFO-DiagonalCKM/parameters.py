# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.2.0 for Mac OS X x86 (64-bit) (December 12, 2020)
# Date: Wed 1 Dec 2021 17:23:56



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
A12t = Parameter(name = 'A12t',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'A_{\\text{12t}}',
                 lhablock = 'A12t',
                 lhacode = [ 1 ])

A12t2t = Parameter(name = 'A12t2t',
                   nature = 'external',
                   type = 'complex',
                   value = 1.,
                   texname = 'A_{\\text{12t2t}}',
                   lhablock = 'A12t2t',
                   lhacode = [ 1 ])

A1t22t = Parameter(name = 'A1t22t',
                   nature = 'external',
                   type = 'complex',
                   value = 1.,
                   texname = 'A_{\\text{1t22t}}',
                   lhablock = 'A1t22t',
                   lhacode = [ 1 ])

A2t3 = Parameter(name = 'A2t3',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'A_{\\text{2t3}}',
                 lhablock = 'A2t3',
                 lhacode = [ 1 ])

m1 = Parameter(name = 'm1',
               nature = 'external',
               type = 'real',
               value = 1000.,
               texname = '\\text{m1}',
               lhablock = 'LQPARAM',
               lhacode = [ 1 ])

m1t = Parameter(name = 'm1t',
                nature = 'external',
                type = 'real',
                value = 2000.,
                texname = '\\text{m1t}',
                lhablock = 'LQPARAM',
                lhacode = [ 2 ])

m2 = Parameter(name = 'm2',
               nature = 'external',
               type = 'real',
               value = 3000.,
               texname = '\\text{m2}',
               lhablock = 'LQPARAM',
               lhacode = [ 3 ])

m2t = Parameter(name = 'm2t',
                nature = 'external',
                type = 'real',
                value = 4000.,
                texname = '\\text{m2t}',
                lhablock = 'LQPARAM',
                lhacode = [ 4 ])

m3 = Parameter(name = 'm3',
               nature = 'external',
               type = 'real',
               value = 5000.,
               texname = '\\text{m3}',
               lhablock = 'LQPARAM',
               lhacode = [ 5 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

Y1 = Parameter(name = 'Y1',
               nature = 'external',
               type = 'complex',
               value = 1.,
               texname = 'Y_1',
               lhablock = 'Y1',
               lhacode = [ 1 ])

Y11t2 = Parameter(name = 'Y11t2',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{11t2}}',
                  lhablock = 'Y11t2',
                  lhacode = [ 1 ])

Y11t2t2 = Parameter(name = 'Y11t2t2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = 'Y_{\\text{11t2t2}}',
                    lhablock = 'Y11t2t2',
                    lhacode = [ 1 ])

Y11t2t2prime = Parameter(name = 'Y11t2t2prime',
                         nature = 'external',
                         type = 'complex',
                         value = 1.,
                         texname = '\\text{Yp}_{\\text{11t2t2}}',
                         lhablock = 'Y11t2t2prime',
                         lhacode = [ 1 ])

Y1223 = Parameter(name = 'Y1223',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{1223}',
                  lhablock = 'Y1223',
                  lhacode = [ 1 ])

Y1223prime = Parameter(name = 'Y1223prime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{1223}',
                       lhablock = 'Y1223prime',
                       lhacode = [ 1 ])

Y123 = Parameter(name = 'Y123',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{123}',
                 lhablock = 'Y123',
                 lhacode = [ 1 ])

Y12t2t3 = Parameter(name = 'Y12t2t3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = 'Y_{\\text{12t2t3}}',
                    lhablock = 'Y12t2t3',
                    lhacode = [ 1 ])

Y12t2t3prime = Parameter(name = 'Y12t2t3prime',
                         nature = 'external',
                         type = 'complex',
                         value = 1.,
                         texname = '\\text{Yp}_{\\text{12t2t3}}',
                         lhablock = 'Y12t2t3prime',
                         lhacode = [ 1 ])

Y12t3 = Parameter(name = 'Y12t3',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{12t3}}',
                  lhablock = 'Y12t3',
                  lhacode = [ 1 ])

Y13 = Parameter(name = 'Y13',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_{13}',
                lhablock = 'Y13',
                lhacode = [ 1 ])

Y1313 = Parameter(name = 'Y1313',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{1313}',
                  lhablock = 'Y1313',
                  lhacode = [ 1 ])

Y1333 = Parameter(name = 'Y1333',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{1333}',
                  lhablock = 'Y1333',
                  lhacode = [ 1 ])

Y1t = Parameter(name = 'Y1t',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_{\\text{1t}}',
                lhablock = 'Y1t',
                lhacode = [ 1 ])

Y1t23 = Parameter(name = 'Y1t23',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{1t23}}',
                  lhablock = 'Y1t23',
                  lhacode = [ 1 ])

Y1t2t23prime = Parameter(name = 'Y1t2t23prime',
                         nature = 'external',
                         type = 'complex',
                         value = 1.,
                         texname = '\\text{Yp}_{\\text{1t2t23}}',
                         lhablock = 'Y1t2t23prime',
                         lhacode = [ 1 ])

Y1t3 = Parameter(name = 'Y1t3',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{\\text{1t3}}',
                 lhablock = 'Y1t3',
                 lhacode = [ 1 ])

Y1t2t23 = Parameter(name = 'Y1t2t23',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = 'Y_{\\text{1t2t23}}',
                    lhablock = 'Y1t32t23',
                    lhacode = [ 1 ])

Y2 = Parameter(name = 'Y2',
               nature = 'external',
               type = 'complex',
               value = 1.,
               texname = 'Y_2',
               lhablock = 'Y2',
               lhacode = [ 1 ])

Y22 = Parameter(name = 'Y22',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_{22}',
                lhablock = 'Y22',
                lhacode = [ 1 ])

Y233 = Parameter(name = 'Y233',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{233}',
                 lhablock = 'Y223',
                 lhacode = [ 1 ])

Y22t = Parameter(name = 'Y22t',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{\\text{22t}}',
                 lhablock = 'Y22t',
                 lhacode = [ 1 ])

Y2t = Parameter(name = 'Y2t',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_{\\text{2t}}',
                lhablock = 'Y2t',
                lhacode = [ 1 ])

Y2t33 = Parameter(name = 'Y2t33',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{2t33}}',
                  lhablock = 'Y2t23',
                  lhacode = [ 1 ])

Y2t2t = Parameter(name = 'Y2t2t',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{2t2t}}',
                  lhablock = 'Y2t2t',
                  lhacode = [ 1 ])

Y3 = Parameter(name = 'Y3',
               nature = 'external',
               type = 'complex',
               value = 1.,
               texname = 'Y_3',
               lhablock = 'Y3',
               lhacode = [ 1 ])

Y33 = Parameter(name = 'Y33',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_{33}',
                lhablock = 'Y33',
                lhacode = [ 1 ])

Yf3 = Parameter(name = 'Yf3',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_3{}^{\\text{(5)}}',
                lhablock = 'Yf3',
                lhacode = [ 1 ])

Yo1 = Parameter(name = 'Yo1',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_1{}^{\\text{(1)}}',
                lhablock = 'Yo1',
                lhacode = [ 1 ])

Yo11t = Parameter(name = 'Yo11t',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{11t}}{}^{\\text{(1)}}',
                  lhablock = 'Yo11t',
                  lhacode = [ 1 ])

Yo11tprime = Parameter(name = 'Yo11tprime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{11t}}{}^{\\text{(1)}}',
                       lhablock = 'Yo11tprime',
                       lhacode = [ 1 ])

Yo12 = Parameter(name = 'Yo12',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{12}{}^{\\text{(1)}}',
                 lhablock = 'Yo12',
                 lhacode = [ 1 ])

Yo12prime = Parameter(name = 'Yo12prime',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Yp}_{12}{}^{\\text{(1)}}',
                      lhablock = 'Yo12prime',
                      lhacode = [ 1 ])

Yo12t = Parameter(name = 'Yo12t',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{12t}}{}^{\\text{(1)}}',
                  lhablock = 'Yo12t',
                  lhacode = [ 1 ])

Yo12tprime = Parameter(name = 'Yo12tprime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{12t}}{}^{\\text{(1)}}',
                       lhablock = 'Yo12tprime',
                       lhacode = [ 1 ])

Yo13 = Parameter(name = 'Yo13',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{13}{}^{\\text{(1)}}',
                 lhablock = 'Yo13',
                 lhacode = [ 1 ])

Yo13prime = Parameter(name = 'Yo13prime',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Yp}_{13}{}^{\\text{(1)}}',
                      lhablock = 'Yo13prime',
                      lhacode = [ 1 ])

Yo1t = Parameter(name = 'Yo1t',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{\\text{1t}}{}^{\\text{(1)}}',
                 lhablock = 'Yo1t',
                 lhacode = [ 1 ])

Yo1t2 = Parameter(name = 'Yo1t2',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{1t2}}{}^{\\text{(1)}}',
                  lhablock = 'Yo1t2',
                  lhacode = [ 1 ])

Yo1t2prime = Parameter(name = 'Yo1t2prime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{1t2}}{}^{\\text{(1)}}',
                       lhablock = 'Yo1t2prime',
                       lhacode = [ 1 ])

Yo1t2t = Parameter(name = 'Yo1t2t',
                   nature = 'external',
                   type = 'complex',
                   value = 1.,
                   texname = 'Y_{\\text{1t2t}}{}^{\\text{(1)}}',
                   lhablock = 'Yo1t2t',
                   lhacode = [ 1 ])

Yo1t2tprime = Parameter(name = 'Yo1t2tprime',
                        nature = 'external',
                        type = 'complex',
                        value = 1.,
                        texname = '\\text{Yp}_{\\text{1t2t}}{}^{\\text{(1)}}',
                        lhablock = 'Yo1t2tprime',
                        lhacode = [ 1 ])

Yo1t3 = Parameter(name = 'Yo1t3',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{1t3}}{}^{\\text{(1)}}',
                  lhablock = 'Yo1t3',
                  lhacode = [ 1 ])

Yo1t3prime = Parameter(name = 'Yo1t3prime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{1t3}}{}^{\\text{(1)}}',
                       lhablock = 'Yo1t3prime',
                       lhacode = [ 1 ])

Yo2 = Parameter(name = 'Yo2',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_2{}^{\\text{(1)}}',
                lhablock = 'Yo2',
                lhacode = [ 1 ])

Yo22t = Parameter(name = 'Yo22t',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{22t}}{}^{\\text{(1)}}',
                  lhablock = 'Yo22t',
                  lhacode = [ 1 ])

Yo22tprime = Parameter(name = 'Yo22tprime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{22t}}{}^{\\text{(1)}}',
                       lhablock = 'Yo22tprime',
                       lhacode = [ 1 ])

Yo23 = Parameter(name = 'Yo23',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{23}{}^{\\text{(1)}}',
                 lhablock = 'Yo23',
                 lhacode = [ 1 ])

Yo23prime = Parameter(name = 'Yo23prime',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Yp}_{23}{}^{\\text{(1)}}',
                      lhablock = 'Yo23prime',
                      lhacode = [ 1 ])

Yo2t = Parameter(name = 'Yo2t',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{\\text{2t}}{}^{\\text{(1)}}',
                 lhablock = 'Yo2t',
                 lhacode = [ 1 ])

Yo2t3 = Parameter(name = 'Yo2t3',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{2t3}}{}^{\\text{(1)}}',
                  lhablock = 'Yo2t3',
                  lhacode = [ 1 ])

Yo2t3prime = Parameter(name = 'Yo2t3prime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{2t3}}{}^{\\text{(1)}}',
                       lhablock = 'Yo2t3prime',
                       lhacode = [ 1 ])

Yo3 = Parameter(name = 'Yo3',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_3{}^{\\text{(1)}}',
                lhablock = 'Yo3',
                lhacode = [ 1 ])

Yt2 = Parameter(name = 'Yt2',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_2{}^{\\text{(3)}}',
                lhablock = 'Yt2',
                lhacode = [ 1 ])

Yt22t = Parameter(name = 'Yt22t',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{22t}}{}^{\\text{(3)}}',
                  lhablock = 'Yt22t',
                  lhacode = [ 1 ])

Yt22tprime = Parameter(name = 'Yt22tprime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{22t}}{}^{\\text{(3)}}',
                       lhablock = 'Yt22tprime',
                       lhacode = [ 1 ])

Yt23 = Parameter(name = 'Yt23',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{23}{}^{\\text{(3)}}',
                 lhablock = 'Yt23',
                 lhacode = [ 1 ])

Yt23prime = Parameter(name = 'Yt23prime',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Yp}_{23}{}^{\\text{(3)}}',
                      lhablock = 'Yt23prime',
                      lhacode = [ 1 ])

Yt2t = Parameter(name = 'Yt2t',
                 nature = 'external',
                 type = 'complex',
                 value = 1.,
                 texname = 'Y_{\\text{2t}}{}^{\\text{(3)}}',
                 lhablock = 'Yt2t',
                 lhacode = [ 1 ])

Yt2t3 = Parameter(name = 'Yt2t3',
                  nature = 'external',
                  type = 'complex',
                  value = 1.,
                  texname = 'Y_{\\text{2t3}}{}^{\\text{(3)}}',
                  lhablock = 'Yt2t3',
                  lhacode = [ 1 ])

Yt2t3prime = Parameter(name = 'Yt2t3prime',
                       nature = 'external',
                       type = 'complex',
                       value = 1.,
                       texname = '\\text{Yp}_{\\text{2t3}}{}^{\\text{(3)}}',
                       lhablock = 'Yt2t3prime',
                       lhacode = [ 1 ])

Yt3 = Parameter(name = 'Yt3',
                nature = 'external',
                type = 'complex',
                value = 1.,
                texname = 'Y_3{}^{\\text{(3)}}',
                lhablock = 'Yt3',
                lhacode = [ 1 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

Y2LR1x1 = Parameter(name = 'Y2LR1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2LR1x1}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 1, 1 ])

Y2LR1x2 = Parameter(name = 'Y2LR1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR1x2}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 1, 2 ])

Y2LR1x3 = Parameter(name = 'Y2LR1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR1x3}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 1, 3 ])

Y2LR2x1 = Parameter(name = 'Y2LR2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR2x1}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 2, 1 ])

Y2LR2x2 = Parameter(name = 'Y2LR2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2LR2x2}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 2, 2 ])

Y2LR2x3 = Parameter(name = 'Y2LR2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR2x3}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 2, 3 ])

Y2LR3x1 = Parameter(name = 'Y2LR3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR3x1}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 3, 1 ])

Y2LR3x2 = Parameter(name = 'Y2LR3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2LR3x2}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 3, 2 ])

Y2LR3x3 = Parameter(name = 'Y2LR3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2LR3x3}',
                    lhablock = 'YUKR2LR',
                    lhacode = [ 3, 3 ])

Y2RL1x1 = Parameter(name = 'Y2RL1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2RL1x1}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 1, 1 ])

Y2RL1x2 = Parameter(name = 'Y2RL1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL1x2}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 1, 2 ])

Y2RL1x3 = Parameter(name = 'Y2RL1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL1x3}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 1, 3 ])

Y2RL2x1 = Parameter(name = 'Y2RL2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL2x1}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 2, 1 ])

Y2RL2x2 = Parameter(name = 'Y2RL2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2RL2x2}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 2, 2 ])

Y2RL2x3 = Parameter(name = 'Y2RL2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL2x3}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 2, 3 ])

Y2RL3x1 = Parameter(name = 'Y2RL3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL3x1}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 3, 1 ])

Y2RL3x2 = Parameter(name = 'Y2RL3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y2RL3x2}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 3, 2 ])

Y2RL3x3 = Parameter(name = 'Y2RL3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y2RL3x3}',
                    lhablock = 'YUKR2RL',
                    lhacode = [ 3, 3 ])

Y2tRL1x1 = Parameter(name = 'Y2tRL1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y2tRL1x1}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 1, 1 ])

Y2tRL1x2 = Parameter(name = 'Y2tRL1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL1x2}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 1, 2 ])

Y2tRL1x3 = Parameter(name = 'Y2tRL1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL1x3}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 1, 3 ])

Y2tRL2x1 = Parameter(name = 'Y2tRL2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL2x1}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 2, 1 ])

Y2tRL2x2 = Parameter(name = 'Y2tRL2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y2tRL2x2}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 2, 2 ])

Y2tRL2x3 = Parameter(name = 'Y2tRL2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL2x3}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 2, 3 ])

Y2tRL3x1 = Parameter(name = 'Y2tRL3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL3x1}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 3, 1 ])

Y2tRL3x2 = Parameter(name = 'Y2tRL3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y2tRL3x2}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 3, 2 ])

Y2tRL3x3 = Parameter(name = 'Y2tRL3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y2tRL3x3}',
                     lhablock = 'YUKR2t',
                     lhacode = [ 3, 3 ])

Y1LL1x1 = Parameter(name = 'Y1LL1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1LL1x1}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 1, 1 ])

Y1LL1x2 = Parameter(name = 'Y1LL1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL1x2}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 1, 2 ])

Y1LL1x3 = Parameter(name = 'Y1LL1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL1x3}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 1, 3 ])

Y1LL2x1 = Parameter(name = 'Y1LL2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL2x1}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 2, 1 ])

Y1LL2x2 = Parameter(name = 'Y1LL2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1LL2x2}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 2, 2 ])

Y1LL2x3 = Parameter(name = 'Y1LL2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL2x3}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 2, 3 ])

Y1LL3x1 = Parameter(name = 'Y1LL3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL3x1}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 3, 1 ])

Y1LL3x2 = Parameter(name = 'Y1LL3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1LL3x2}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 3, 2 ])

Y1LL3x3 = Parameter(name = 'Y1LL3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1LL3x3}',
                    lhablock = 'YUKS1LL',
                    lhacode = [ 3, 3 ])

Y1QLL1x1 = Parameter(name = 'Y1QLL1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QLL1x1}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 1, 1 ])

Y1QLL1x2 = Parameter(name = 'Y1QLL1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL1x2}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 1, 2 ])

Y1QLL1x3 = Parameter(name = 'Y1QLL1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL1x3}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 1, 3 ])

Y1QLL2x1 = Parameter(name = 'Y1QLL2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL2x1}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 2, 1 ])

Y1QLL2x2 = Parameter(name = 'Y1QLL2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QLL2x2}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 2, 2 ])

Y1QLL2x3 = Parameter(name = 'Y1QLL2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL2x3}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 2, 3 ])

Y1QLL3x1 = Parameter(name = 'Y1QLL3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL3x1}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 3, 1 ])

Y1QLL3x2 = Parameter(name = 'Y1QLL3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QLL3x2}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 3, 2 ])

Y1QLL3x3 = Parameter(name = 'Y1QLL3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QLL3x3}',
                     lhablock = 'YUKS1QLL',
                     lhacode = [ 3, 3 ])

Y1QRR1x1 = Parameter(name = 'Y1QRR1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QRR1x1}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 1, 1 ])

Y1QRR1x2 = Parameter(name = 'Y1QRR1x2',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y1QRR1x2}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 1, 2 ])

Y1QRR1x3 = Parameter(name = 'Y1QRR1x3',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y1QRR1x3}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 1, 3 ])

Y1QRR2x1 = Parameter(name = 'Y1QRR2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QRR2x1}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 2, 1 ])

Y1QRR2x2 = Parameter(name = 'Y1QRR2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QRR2x2}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 2, 2 ])

Y1QRR2x3 = Parameter(name = 'Y1QRR2x3',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y1QRR2x3}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 2, 3 ])

Y1QRR3x1 = Parameter(name = 'Y1QRR3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QRR3x1}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 3, 1 ])

Y1QRR3x2 = Parameter(name = 'Y1QRR3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1QRR3x2}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 3, 2 ])

Y1QRR3x3 = Parameter(name = 'Y1QRR3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1QRR3x3}',
                     lhablock = 'YUKS1QRR',
                     lhacode = [ 3, 3 ])

Y1RR1x1 = Parameter(name = 'Y1RR1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1RR1x1}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 1, 1 ])

Y1RR1x2 = Parameter(name = 'Y1RR1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR1x2}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 1, 2 ])

Y1RR1x3 = Parameter(name = 'Y1RR1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR1x3}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 1, 3 ])

Y1RR2x1 = Parameter(name = 'Y1RR2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR2x1}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 2, 1 ])

Y1RR2x2 = Parameter(name = 'Y1RR2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1RR2x2}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 2, 2 ])

Y1RR2x3 = Parameter(name = 'Y1RR2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR2x3}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 2, 3 ])

Y1RR3x1 = Parameter(name = 'Y1RR3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR3x1}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 3, 1 ])

Y1RR3x2 = Parameter(name = 'Y1RR3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y1RR3x2}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 3, 2 ])

Y1RR3x3 = Parameter(name = 'Y1RR3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y1RR3x3}',
                    lhablock = 'YUKS1RR',
                    lhacode = [ 3, 3 ])

Y1tQRR1x1 = Parameter(name = 'Y1tQRR1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Y1tQRR1x1}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 1, 1 ])

Y1tQRR1x2 = Parameter(name = 'Y1tQRR1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR1x2}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 1, 2 ])

Y1tQRR1x3 = Parameter(name = 'Y1tQRR1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR1x3}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 1, 3 ])

Y1tQRR2x1 = Parameter(name = 'Y1tQRR2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR2x1}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 2, 1 ])

Y1tQRR2x2 = Parameter(name = 'Y1tQRR2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Y1tQRR2x2}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 2, 2 ])

Y1tQRR2x3 = Parameter(name = 'Y1tQRR2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR2x3}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 2, 3 ])

Y1tQRR3x1 = Parameter(name = 'Y1tQRR3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR3x1}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 3, 1 ])

Y1tQRR3x2 = Parameter(name = 'Y1tQRR3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Y1tQRR3x2}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 3, 2 ])

Y1tQRR3x3 = Parameter(name = 'Y1tQRR3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 1.,
                      texname = '\\text{Y1tQRR3x3}',
                      lhablock = 'YUKS1tQRR',
                      lhacode = [ 3, 3 ])

Y1tRR1x1 = Parameter(name = 'Y1tRR1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1tRR1x1}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 1, 1 ])

Y1tRR1x2 = Parameter(name = 'Y1tRR1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR1x2}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 1, 2 ])

Y1tRR1x3 = Parameter(name = 'Y1tRR1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR1x3}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 1, 3 ])

Y1tRR2x1 = Parameter(name = 'Y1tRR2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR2x1}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 2, 1 ])

Y1tRR2x2 = Parameter(name = 'Y1tRR2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1tRR2x2}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 2, 2 ])

Y1tRR2x3 = Parameter(name = 'Y1tRR2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR2x3}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 2, 3 ])

Y1tRR3x1 = Parameter(name = 'Y1tRR3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR3x1}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 3, 1 ])

Y1tRR3x2 = Parameter(name = 'Y1tRR3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y1tRR3x2}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 3, 2 ])

Y1tRR3x3 = Parameter(name = 'Y1tRR3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y1tRR3x3}',
                     lhablock = 'YUKS1tRR',
                     lhacode = [ 3, 3 ])

Y3LL1x1 = Parameter(name = 'Y3LL1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y3LL1x1}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 1, 1 ])

Y3LL1x2 = Parameter(name = 'Y3LL1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL1x2}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 1, 2 ])

Y3LL1x3 = Parameter(name = 'Y3LL1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL1x3}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 1, 3 ])

Y3LL2x1 = Parameter(name = 'Y3LL2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL2x1}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 2, 1 ])

Y3LL2x2 = Parameter(name = 'Y3LL2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y3LL2x2}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 2, 2 ])

Y3LL2x3 = Parameter(name = 'Y3LL2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL2x3}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 2, 3 ])

Y3LL3x1 = Parameter(name = 'Y3LL3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL3x1}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 3, 1 ])

Y3LL3x2 = Parameter(name = 'Y3LL3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Y3LL3x2}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 3, 2 ])

Y3LL3x3 = Parameter(name = 'Y3LL3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1.,
                    texname = '\\text{Y3LL3x3}',
                    lhablock = 'YUKS3LL',
                    lhacode = [ 3, 3 ])

Y3QLL1x1 = Parameter(name = 'Y3QLL1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y3QLL1x1}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 1, 1 ])

Y3QLL1x2 = Parameter(name = 'Y3QLL1x2',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y3QLL1x2}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 1, 2 ])

Y3QLL1x3 = Parameter(name = 'Y3QLL1x3',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y3QLL1x3}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 1, 3 ])

Y3QLL2x1 = Parameter(name = 'Y3QLL2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y3QLL2x1}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 2, 1 ])

Y3QLL2x2 = Parameter(name = 'Y3QLL2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y3QLL2x2}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 2, 2 ])

Y3QLL2x3 = Parameter(name = 'Y3QLL2x3',
                     nature = 'external',
                     type = 'complex',
                     value = -1.,
                     texname = '\\text{Y3QLL2x3}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 2, 3 ])

Y3QLL3x1 = Parameter(name = 'Y3QLL3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y3QLL3x1}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 3, 1 ])

Y3QLL3x2 = Parameter(name = 'Y3QLL3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 1.,
                     texname = '\\text{Y3QLL3x2}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 3, 2 ])

Y3QLL3x3 = Parameter(name = 'Y3QLL3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Y3QLL3x3}',
                     lhablock = 'YUKS3QLL',
                     lhacode = [ 3, 3 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

W23mat1x1 = Parameter(name = 'W23mat1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '1.',
                      texname = '\\text{W23mat1x1}')

W23mat1x3 = Parameter(name = 'W23mat1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '0.',
                      texname = '\\text{W23mat1x3}')

W23mat3x1 = Parameter(name = 'W23mat3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '0.',
                      texname = '\\text{W23mat3x1}')

W43mat1x1 = Parameter(name = 'W43mat1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '1.',
                      texname = '\\text{W43mat1x1}')

W43mat2x2 = Parameter(name = 'W43mat2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '1.',
                      texname = '\\text{W43mat2x2}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

m1m13hat = Parameter(name = 'm1m13hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m1**2 + (vev**2*(Y1 - abs(A12t)**2/(-m1**2 + m2t**2)))/2.)',
                     texname = '\\text{m1m13hat}')

m1tm43hat = Parameter(name = 'm1tm43hat',
                      nature = 'internal',
                      type = 'real',
                      value = 'cmath.sqrt(m1t**2 + (vev**2*Y1t)/2.)',
                      texname = '\\text{m1tm43hat}')

m2p23hat = Parameter(name = 'm2p23hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m2**2 + (vev**2*Y2)/2.)',
                     texname = '\\text{m2p23hat}')

m2p53hat = Parameter(name = 'm2p53hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m2**2 + (vev**2*(Y2 + Y22))/2.)',
                     texname = '\\text{m2p53hat}')

m2tm13hat = Parameter(name = 'm2tm13hat',
                      nature = 'internal',
                      type = 'real',
                      value = 'cmath.sqrt(m2t**2 + (vev**2*(Y2t + abs(A12t)**2/(-m1**2 + m2t**2) + abs(A2t3)**2/(m2t**2 - m3**2)))/2.)',
                      texname = '\\text{m2tm13hat}')

m2tp23hat = Parameter(name = 'm2tp23hat',
                      nature = 'internal',
                      type = 'real',
                      value = 'cmath.sqrt(m2t**2 + (vev**2*(Y2t + Y2t2t + (2*abs(A2t3)**2)/(m2t**2 - m3**2)))/2.)',
                      texname = '\\text{m2tp23hat}')

m3m13hat = Parameter(name = 'm3m13hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m3**2 + (vev**2*(Y3 - abs(A2t3)**2/(m2t**2 - m3**2)))/2.)',
                     texname = '\\text{m3m13hat}')

m3m43hat = Parameter(name = 'm3m43hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m3**2 + (vev**2*(Y3 - Y33))/2.)',
                     texname = '\\text{m3m43hat}')

m3p23hat = Parameter(name = 'm3p23hat',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(m3**2 + (vev**2*(Y3 + Y33 - (2*abs(A2t3)**2)/(m2t**2 - m3**2)))/2.)',
                     texname = '\\text{m3p23hat}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

W13mat1x1 = Parameter(name = 'W13mat1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '1 - (vev**2*abs(A12t)**2)/(4.*(m1**2 - m2t**2)**2)',
                      texname = '\\text{W13mat1x1}')

W13mat1x2 = Parameter(name = 'W13mat1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev*complexconjugate(A12t))/((m1**2 - m2t**2)*cmath.sqrt(2))',
                      texname = '\\text{W13mat1x2}')

W13mat1x3 = Parameter(name = 'W13mat1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev**2*((m1**2 - m2t**2)*Y13 + A2t3*complexconjugate(A12t)))/(2.*(m1**2 - m2t**2)*(m1**2 - m3**2))',
                      texname = '\\text{W13mat1x3}')

W13mat2x1 = Parameter(name = 'W13mat2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '-((A12t*vev)/((m1**2 - m2t**2)*cmath.sqrt(2)))',
                      texname = '\\text{W13mat2x1}')

W13mat2x2 = Parameter(name = 'W13mat2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '1 - (vev**2*(abs(A12t)**2/(m1**2 - m2t**2)**2 + abs(A2t3)**2/(-m2t**2 + m3**2)**2))/4.',
                      texname = '\\text{W13mat2x2}')

W13mat2x3 = Parameter(name = 'W13mat2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '-((A2t3*vev)/(-m2t**2 + m3**2*cmath.sqrt(2)))',
                      texname = '\\text{W13mat2x3}')

W13mat3x1 = Parameter(name = 'W13mat3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '-0.5*(vev**2*(A12t*complexconjugate(A2t3) + (-m2t**2 + m3**2)*complexconjugate(Y13)))/((m1**2 - m3**2)*(-m2t**2 + m3**2))',
                      texname = '\\text{W13mat3x1}')

W13mat3x2 = Parameter(name = 'W13mat3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev*complexconjugate(A2t3))/((-m2t**2 + m3**2)*cmath.sqrt(2))',
                      texname = '\\text{W13mat3x2}')

W13mat3x3 = Parameter(name = 'W13mat3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '1 - (vev**2*abs(A2t3)**2)/(4.*(-m2t**2 + m3**2)**2)',
                      texname = '\\text{W13mat3x3}')

W23mat1x2 = Parameter(name = 'W23mat1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev**2*Y22t)/(2.*(m2**2 - m2t**2))',
                      texname = '\\text{W23mat1x2}')

W23mat2x1 = Parameter(name = 'W23mat2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '-0.5*(vev*complexconjugate(Y22t))/(m2**2 - m2t**2)',
                      texname = '\\text{W23mat2x1}')

W23mat2x2 = Parameter(name = 'W23mat2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '1. - (vev**2*abs(A2t3)**2)/(2.*(-m2t**2 + m3**2)**2)',
                      texname = '\\text{W23mat2x2}')

W23mat2x3 = Parameter(name = 'W23mat2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '-((A2t3*vev)/(m2t**2 - m3**2))',
                      texname = '\\text{W23mat2x3}')

W23mat3x2 = Parameter(name = 'W23mat3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev*complexconjugate(A2t3))/(m2t**2 - m3**2)',
                      texname = '\\text{W23mat3x2}')

W23mat3x3 = Parameter(name = 'W23mat3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '1. - (vev**2*abs(A2t3)**2)/(2.*(-m2t**2 + m3**2)**2)',
                      texname = '\\text{W23mat3x3}')

W43mat1x2 = Parameter(name = 'W43mat1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(vev**2*complexconjugate(Y1t3))/((m1t**2 - m3**2)*cmath.sqrt(2))',
                      texname = '\\text{W43mat1x2}')

W43mat2x1 = Parameter(name = 'W43mat2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '-((vev**2*Y1t3)/((m1t**2 - m3**2)*cmath.sqrt(2)))',
                      texname = '\\text{W43mat2x1}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

W1m13hat = Parameter(name = 'W1m13hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(m1m13hat*(abs(W13mat1x1*Y1LL1x1 + W13mat1x3*Y1LL1x1)**2 + abs(W13mat1x1*Y1LL1x2 + W13mat1x3*Y1LL1x2)**2 + abs(W13mat1x1*Y1LL1x3 + W13mat1x3*Y1LL1x3)**2 + abs(W13mat1x1*Y1LL2x1 + W13mat1x3*Y1LL2x1)**2 + abs(W13mat1x1*Y1LL2x2 + W13mat1x3*Y1LL2x2)**2 + abs(W13mat1x1*Y1LL2x3 + W13mat1x3*Y1LL2x3)**2 + abs(W13mat1x1*Y1LL3x1 + W13mat1x3*Y1LL3x1)**2 + abs(W13mat1x1*Y1LL3x2 + W13mat1x3*Y1LL3x2)**2 + abs(W13mat1x1*Y1LL3x3 + W13mat1x3*Y1LL3x3)**2 + 2*abs(W13mat1x1*Y1QRR1x1)**2 + 2*abs(W13mat1x1*Y1QRR1x2)**2 + 2*abs(W13mat1x1*Y1QRR1x3)**2 + 2*abs(W13mat1x1*Y1QRR2x1)**2 + 2*abs(W13mat1x1*Y1QRR2x2)**2 + 2*abs(W13mat1x1*Y1QRR2x3)**2 + abs(W13mat1x1*Y1RR1x1)**2 + abs(W13mat1x1*Y1RR1x2)**2 + abs(W13mat1x1*Y1RR2x1)**2 + abs(W13mat1x1*Y1RR2x2)**2 + abs(W13mat1x1*Y1RR3x1)**2 + abs(W13mat1x1*Y1RR3x2)**2 + abs(W13mat1x2*Y2tRL1x1)**2 + abs(W13mat1x2*Y2tRL1x2)**2 + abs(W13mat1x2*Y2tRL1x3)**2 + abs(W13mat1x2*Y2tRL2x1)**2 + abs(W13mat1x2*Y2tRL2x2)**2 + abs(W13mat1x2*Y2tRL2x3)**2 + abs(W13mat1x2*Y2tRL3x1)**2 + abs(W13mat1x2*Y2tRL3x2)**2 + abs(W13mat1x2*Y2tRL3x3)**2 + abs(W13mat1x1*Y1LL1x1 - W13mat1x3*Y3LL1x1)**2 + abs(W13mat1x1*Y1LL1x2 - W13mat1x3*Y3LL1x2)**2 + abs(W13mat1x1*Y1LL2x1 - W13mat1x3*Y3LL2x1)**2 + abs(W13mat1x1*Y1LL2x2 - W13mat1x3*Y3LL2x2)**2 + abs(W13mat1x1*Y1LL3x1 - W13mat1x3*Y3LL3x1)**2 + abs(W13mat1x1*Y1LL3x2 - W13mat1x3*Y3LL3x2)**2 + 8*abs(Y1QLL1x1*complexconjugate(W13mat1x1) - Y1LL1x1*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL1x2*complexconjugate(W13mat1x1) - Y1LL1x2*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL1x3*complexconjugate(W13mat1x1) - Y1LL1x3*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL2x1*complexconjugate(W13mat1x1) - Y1LL2x1*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL2x2*complexconjugate(W13mat1x1) - Y1LL2x2*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL2x3*complexconjugate(W13mat1x1) - Y1LL2x3*complexconjugate(W13mat1x3))**2) + ((m1m13hat**2 - ymt**2)**2*(2*abs(W13mat1x1*Y1QRR3x1)**2 + 2*abs(W13mat1x1*Y1QRR3x2)**2 + 2*abs(W13mat1x1*Y1QRR3x3)**2 + abs(W13mat1x1*Y1RR1x3)**2 + abs(W13mat1x1*Y1RR2x3)**2 + abs(W13mat1x1*Y1RR3x3)**2 + abs(W13mat1x1*Y1LL1x3 - W13mat1x3*Y3LL1x3)**2 + abs(W13mat1x1*Y1LL2x3 - W13mat1x3*Y3LL2x3)**2 + abs(W13mat1x1*Y1LL3x3 - W13mat1x3*Y3LL3x3)**2 + 8*abs(Y1QLL3x1*complexconjugate(W13mat1x1) - Y1LL3x1*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL3x2*complexconjugate(W13mat1x1) - Y1LL3x2*complexconjugate(W13mat1x3))**2 + 8*abs(Y1QLL3x3*complexconjugate(W13mat1x1) - Y1LL3x3*complexconjugate(W13mat1x3))**2))/m1m13hat**3)/(16.*cmath.pi)',
                     texname = '\\text{W1m13hat}')

W1tm43hat = Parameter(name = 'W1tm43hat',
                      nature = 'internal',
                      type = 'real',
                      value = '(m1tm43hat*(8*(abs(W43mat1x1*Y1tQRR1x1)**2 + abs(W43mat1x1*Y1tQRR1x2)**2 + abs(W43mat1x1*Y1tQRR2x1)**2 + abs(W43mat1x1*Y1tQRR2x2)**2) + abs(W43mat1x1*Y1tRR1x1)**2 + abs(W43mat1x1*Y1tRR1x2)**2 + abs(W43mat1x1*Y1tRR1x3)**2 + abs(W43mat1x1*Y1tRR2x1)**2 + abs(W43mat1x1*Y1tRR2x2)**2 + abs(W43mat1x1*Y1tRR2x3)**2 + abs(W43mat1x1*Y1tRR3x1)**2 + abs(W43mat1x1*Y1tRR3x2)**2 + abs(W43mat1x1*Y1tRR3x3)**2 + 2*(abs(W43mat1x2*Y3LL1x1)**2 + abs(W43mat1x2*Y3LL1x2)**2 + abs(W43mat1x2*Y3LL1x3)**2 + abs(W43mat1x2*Y3LL2x1)**2 + abs(W43mat1x2*Y3LL2x2)**2 + abs(W43mat1x2*Y3LL2x3)**2 + abs(W43mat1x2*Y3LL3x1)**2 + abs(W43mat1x2*Y3LL3x2)**2 + abs(W43mat1x2*Y3LL3x3)**2) + 16*(abs(W43mat1x2*Y3QLL1x1)**2 + abs(W43mat1x2*Y3QLL1x2)**2 + abs(W43mat1x2*Y3QLL2x1)**2 + abs(W43mat1x2*Y3QLL2x2)**2)) + ((m1tm43hat**2 - ymt**2)**2*(8*(abs(W43mat1x1*Y1tQRR1x3)**2 + abs(W43mat1x1*Y1tQRR2x3)**2) + 8*(abs(W43mat1x1*Y1tQRR3x1)**2 + abs(W43mat1x1*Y1tQRR3x2)**2) + 16*(abs(W43mat1x2*Y3QLL1x3)**2 + abs(W43mat1x2*Y3QLL2x3)**2) + 16*(abs(W43mat1x2*Y3QLL3x1)**2 + abs(W43mat1x2*Y3QLL3x2)**2)))/m1tm43hat**3 + ((m1tm43hat**2 - 4*ymt**2)**2*(8*abs(W43mat1x1*Y1tQRR3x3)**2 + 16*abs(W43mat1x2*Y3QLL3x3)**2))/m1tm43hat**3)/(16.*cmath.pi)',
                      texname = '\\text{W1tm43hat}')

W2p23hat = Parameter(name = 'W2p23hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(((m2p23hat**2 - ymt**2)**2*(abs(W23mat1x1*Y2RL3x1)**2 + abs(W23mat1x1*Y2RL3x2)**2 + abs(W23mat1x1*Y2RL3x3)**2 + 2*abs(W23mat1x3*Y3LL3x1)**2 + 2*abs(W23mat1x3*Y3LL3x2)**2 + 2*abs(W23mat1x3*Y3LL3x3)**2))/m2p23hat**3 + m2p23hat*(abs(W23mat1x1*Y2LR1x1)**2 + abs(W23mat1x1*Y2LR1x2)**2 + abs(W23mat1x1*Y2LR1x3)**2 + abs(W23mat1x1*Y2LR2x1)**2 + abs(W23mat1x1*Y2LR2x2)**2 + abs(W23mat1x1*Y2LR2x3)**2 + abs(W23mat1x1*Y2LR3x1)**2 + abs(W23mat1x1*Y2LR3x2)**2 + abs(W23mat1x1*Y2LR3x3)**2 + abs(W23mat1x1*Y2RL1x1)**2 + abs(W23mat1x1*Y2RL1x2)**2 + abs(W23mat1x1*Y2RL1x3)**2 + abs(W23mat1x1*Y2RL2x1)**2 + abs(W23mat1x1*Y2RL2x2)**2 + abs(W23mat1x1*Y2RL2x3)**2 + abs(W23mat1x2*Y2tRL1x1)**2 + abs(W23mat1x2*Y2tRL1x2)**2 + abs(W23mat1x2*Y2tRL1x3)**2 + abs(W23mat1x2*Y2tRL2x1)**2 + abs(W23mat1x2*Y2tRL2x2)**2 + abs(W23mat1x2*Y2tRL2x3)**2 + abs(W23mat1x2*Y2tRL3x1)**2 + abs(W23mat1x2*Y2tRL3x2)**2 + abs(W23mat1x2*Y2tRL3x3)**2 + 2*abs(W23mat1x3*Y3LL1x1)**2 + 2*abs(W23mat1x3*Y3LL1x2)**2 + 2*abs(W23mat1x3*Y3LL1x3)**2 + 2*abs(W23mat1x3*Y3LL2x1)**2 + 2*abs(W23mat1x3*Y3LL2x2)**2 + 2*abs(W23mat1x3*Y3LL2x3)**2 + 48*abs(W23mat1x3*Y3QLL1x1)**2 + 48*abs(W23mat1x3*Y3QLL2x2)**2 + 48*abs(W23mat1x3*Y3QLL3x3)**2))/(16.*cmath.pi)',
                     texname = '\\text{W2p23hat}')

W2p53hat = Parameter(name = 'W2p53hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(m2p53hat*(abs(Y2LR1x1)**2 + abs(Y2LR1x2)**2 + abs(Y2LR1x3)**2 + abs(Y2LR2x1)**2 + abs(Y2LR2x2)**2 + abs(Y2LR2x3)**2 + abs(Y2RL1x1)**2 + abs(Y2RL1x2)**2 + abs(Y2RL1x3)**2 + abs(Y2RL2x1)**2 + abs(Y2RL2x2)**2 + abs(Y2RL2x3)**2) + ((m2p53hat**2 - ymt**2)**2*(abs(Y2LR3x1)**2 + abs(Y2LR3x2)**2 + abs(Y2LR3x3)**2 + abs(Y2RL3x1)**2 + abs(Y2RL3x2)**2 + abs(Y2RL3x3)**2))/m2p53hat**3)/(16.*cmath.pi)',
                     texname = '\\text{W2p53hat}')

W2tm13hat = Parameter(name = 'W2tm13hat',
                      nature = 'internal',
                      type = 'real',
                      value = '(m2tm13hat*(abs(W13mat2x1*Y1LL1x1 + W13mat2x3*Y1LL1x1)**2 + abs(W13mat2x1*Y1LL1x2 + W13mat2x3*Y1LL1x2)**2 + abs(W13mat2x1*Y1LL1x3 + W13mat2x3*Y1LL1x3)**2 + abs(W13mat2x1*Y1LL2x1 + W13mat2x3*Y1LL2x1)**2 + abs(W13mat2x1*Y1LL2x2 + W13mat2x3*Y1LL2x2)**2 + abs(W13mat2x1*Y1LL2x3 + W13mat2x3*Y1LL2x3)**2 + abs(W13mat2x1*Y1LL3x1 + W13mat2x3*Y1LL3x1)**2 + abs(W13mat2x1*Y1LL3x2 + W13mat2x3*Y1LL3x2)**2 + abs(W13mat2x1*Y1LL3x3 + W13mat2x3*Y1LL3x3)**2 + 2*abs(W13mat2x1*Y1QRR1x1)**2 + 2*abs(W13mat2x1*Y1QRR1x2)**2 + 2*abs(W13mat2x1*Y1QRR1x3)**2 + 2*abs(W13mat2x1*Y1QRR2x1)**2 + 2*abs(W13mat2x1*Y1QRR2x2)**2 + 2*abs(W13mat2x1*Y1QRR2x3)**2 + abs(W13mat2x1*Y1RR1x1)**2 + abs(W13mat2x1*Y1RR1x2)**2 + abs(W13mat2x1*Y1RR2x1)**2 + abs(W13mat2x1*Y1RR2x2)**2 + abs(W13mat2x1*Y1RR3x1)**2 + abs(W13mat2x1*Y1RR3x2)**2 + abs(W13mat2x2*Y2tRL1x1)**2 + abs(W13mat2x2*Y2tRL1x2)**2 + abs(W13mat2x2*Y2tRL1x3)**2 + abs(W13mat2x2*Y2tRL2x1)**2 + abs(W13mat2x2*Y2tRL2x2)**2 + abs(W13mat2x2*Y2tRL2x3)**2 + abs(W13mat2x2*Y2tRL3x1)**2 + abs(W13mat2x2*Y2tRL3x2)**2 + abs(W13mat2x2*Y2tRL3x3)**2 + abs(W13mat2x1*Y1LL1x1 - W13mat2x3*Y3LL1x1)**2 + abs(W13mat2x1*Y1LL1x2 - W13mat2x3*Y3LL1x2)**2 + abs(W13mat2x1*Y1LL2x1 - W13mat2x3*Y3LL2x1)**2 + abs(W13mat2x1*Y1LL2x2 - W13mat2x3*Y3LL2x2)**2 + abs(W13mat2x1*Y1LL3x1 - W13mat2x3*Y3LL3x1)**2 + abs(W13mat2x1*Y1LL3x2 - W13mat2x3*Y3LL3x2)**2 + 8*abs(Y1QLL1x1*complexconjugate(W13mat2x1) - Y1LL1x1*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL1x2*complexconjugate(W13mat2x1) - Y1LL1x2*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL1x3*complexconjugate(W13mat2x1) - Y1LL1x3*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL2x1*complexconjugate(W13mat2x1) - Y1LL2x1*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL2x2*complexconjugate(W13mat2x1) - Y1LL2x2*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL2x3*complexconjugate(W13mat2x1) - Y1LL2x3*complexconjugate(W13mat2x3))**2) + ((m2tm13hat**2 - ymt**2)**2*(2*abs(W13mat2x1*Y1QRR3x1)**2 + 2*abs(W13mat2x1*Y1QRR3x2)**2 + 2*abs(W13mat2x1*Y1QRR3x3)**2 + abs(W13mat2x1*Y1RR1x3)**2 + abs(W13mat2x1*Y1RR2x3)**2 + abs(W13mat2x1*Y1RR3x3)**2 + abs(W13mat2x1*Y1LL1x3 - W13mat2x3*Y3LL1x3)**2 + abs(W13mat2x1*Y1LL2x3 - W13mat2x3*Y3LL2x3)**2 + abs(W13mat2x1*Y1LL3x3 - W13mat2x3*Y3LL3x3)**2 + 8*abs(Y1QLL3x1*complexconjugate(W13mat2x1) - Y1LL3x1*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL3x2*complexconjugate(W13mat2x1) - Y1LL3x2*complexconjugate(W13mat2x3))**2 + 8*abs(Y1QLL3x3*complexconjugate(W13mat2x1) - Y1LL3x3*complexconjugate(W13mat2x3))**2))/m2tm13hat**3)/(16.*cmath.pi)',
                      texname = '\\text{W2tm13hat}')

W2tp23hat = Parameter(name = 'W2tp23hat',
                      nature = 'internal',
                      type = 'real',
                      value = '(((m2tp23hat**2 - ymt**2)**2*(abs(W23mat2x1*Y2RL3x1)**2 + abs(W23mat2x1*Y2RL3x2)**2 + abs(W23mat2x1*Y2RL3x3)**2 + 2*abs(W23mat2x3*Y3LL3x1)**2 + 2*abs(W23mat2x3*Y3LL3x2)**2 + 2*abs(W23mat2x3*Y3LL3x3)**2))/m2tp23hat**3 + m2tp23hat*(abs(W23mat2x1*Y2LR1x1)**2 + abs(W23mat2x1*Y2LR1x2)**2 + abs(W23mat2x1*Y2LR1x3)**2 + abs(W23mat2x1*Y2LR2x1)**2 + abs(W23mat2x1*Y2LR2x2)**2 + abs(W23mat2x1*Y2LR2x3)**2 + abs(W23mat2x1*Y2LR3x1)**2 + abs(W23mat2x1*Y2LR3x2)**2 + abs(W23mat2x1*Y2LR3x3)**2 + abs(W23mat2x1*Y2RL1x1)**2 + abs(W23mat2x1*Y2RL1x2)**2 + abs(W23mat2x1*Y2RL1x3)**2 + abs(W23mat2x1*Y2RL2x1)**2 + abs(W23mat2x1*Y2RL2x2)**2 + abs(W23mat2x1*Y2RL2x3)**2 + abs(W23mat2x2*Y2tRL1x1)**2 + abs(W23mat2x2*Y2tRL1x2)**2 + abs(W23mat2x2*Y2tRL1x3)**2 + abs(W23mat2x2*Y2tRL2x1)**2 + abs(W23mat2x2*Y2tRL2x2)**2 + abs(W23mat2x2*Y2tRL2x3)**2 + abs(W23mat2x2*Y2tRL3x1)**2 + abs(W23mat2x2*Y2tRL3x2)**2 + abs(W23mat2x2*Y2tRL3x3)**2 + 2*abs(W23mat2x3*Y3LL1x1)**2 + 2*abs(W23mat2x3*Y3LL1x2)**2 + 2*abs(W23mat2x3*Y3LL1x3)**2 + 2*abs(W23mat2x3*Y3LL2x1)**2 + 2*abs(W23mat2x3*Y3LL2x2)**2 + 2*abs(W23mat2x3*Y3LL2x3)**2 + 48*abs(W23mat2x3*Y3QLL1x1)**2 + 48*abs(W23mat2x3*Y3QLL2x2)**2 + 48*abs(W23mat2x3*Y3QLL3x3)**2))/(16.*cmath.pi)',
                      texname = '\\text{W2tp23hat}')

W3m13hat = Parameter(name = 'W3m13hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(m3m13hat*(abs(W13mat3x1*Y1LL1x1 + W13mat3x3*Y1LL1x1)**2 + abs(W13mat3x1*Y1LL1x2 + W13mat3x3*Y1LL1x2)**2 + abs(W13mat3x1*Y1LL1x3 + W13mat3x3*Y1LL1x3)**2 + abs(W13mat3x1*Y1LL2x1 + W13mat3x3*Y1LL2x1)**2 + abs(W13mat3x1*Y1LL2x2 + W13mat3x3*Y1LL2x2)**2 + abs(W13mat3x1*Y1LL2x3 + W13mat3x3*Y1LL2x3)**2 + abs(W13mat3x1*Y1LL3x1 + W13mat3x3*Y1LL3x1)**2 + abs(W13mat3x1*Y1LL3x2 + W13mat3x3*Y1LL3x2)**2 + abs(W13mat3x1*Y1LL3x3 + W13mat3x3*Y1LL3x3)**2 + 2*abs(W13mat3x1*Y1QRR1x1)**2 + 2*abs(W13mat3x1*Y1QRR1x2)**2 + 2*abs(W13mat3x1*Y1QRR1x3)**2 + 2*abs(W13mat3x1*Y1QRR2x1)**2 + 2*abs(W13mat3x1*Y1QRR2x2)**2 + 2*abs(W13mat3x1*Y1QRR2x3)**2 + abs(W13mat3x1*Y1RR1x1)**2 + abs(W13mat3x1*Y1RR1x2)**2 + abs(W13mat3x1*Y1RR2x1)**2 + abs(W13mat3x1*Y1RR2x2)**2 + abs(W13mat3x1*Y1RR3x1)**2 + abs(W13mat3x1*Y1RR3x2)**2 + abs(W13mat3x2*Y2tRL1x1)**2 + abs(W13mat3x2*Y2tRL1x2)**2 + abs(W13mat3x2*Y2tRL1x3)**2 + abs(W13mat3x2*Y2tRL2x1)**2 + abs(W13mat3x2*Y2tRL2x2)**2 + abs(W13mat3x2*Y2tRL2x3)**2 + abs(W13mat3x2*Y2tRL3x1)**2 + abs(W13mat3x2*Y2tRL3x2)**2 + abs(W13mat3x2*Y2tRL3x3)**2 + abs(W13mat3x1*Y1LL1x1 - W13mat1x3*Y3LL1x1)**2 + abs(W13mat3x1*Y1LL1x2 - W13mat1x3*Y3LL1x2)**2 + abs(W13mat3x1*Y1LL2x1 - W13mat1x3*Y3LL2x1)**2 + abs(W13mat3x1*Y1LL2x2 - W13mat1x3*Y3LL2x2)**2 + abs(W13mat3x1*Y1LL3x1 - W13mat1x3*Y3LL3x1)**2 + abs(W13mat3x1*Y1LL3x2 - W13mat1x3*Y3LL3x2)**2 + 8*abs(Y1QLL1x1*complexconjugate(W13mat3x1) - Y1LL1x1*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL1x2*complexconjugate(W13mat3x1) - Y1LL1x2*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL1x3*complexconjugate(W13mat3x1) - Y1LL1x3*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL2x1*complexconjugate(W13mat3x1) - Y1LL2x1*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL2x2*complexconjugate(W13mat3x1) - Y1LL2x2*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL2x3*complexconjugate(W13mat3x1) - Y1LL2x3*complexconjugate(W13mat3x3))**2) + ((m3m13hat**2 - ymt**2)**2*(2*abs(W13mat3x1*Y1QRR3x1)**2 + 2*abs(W13mat3x1*Y1QRR3x2)**2 + 2*abs(W13mat3x1*Y1QRR3x3)**2 + abs(W13mat3x1*Y1RR1x3)**2 + abs(W13mat3x1*Y1RR2x3)**2 + abs(W13mat3x1*Y1RR3x3)**2 + abs(W13mat1x1*Y1LL1x3 - W13mat3x3*Y3LL1x3)**2 + abs(W13mat1x1*Y1LL2x3 - W13mat3x3*Y3LL2x3)**2 + abs(W13mat1x1*Y1LL3x3 - W13mat3x3*Y3LL3x3)**2 + 8*abs(Y1QLL3x1*complexconjugate(W13mat3x1) - Y1LL3x1*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL3x2*complexconjugate(W13mat3x1) - Y1LL3x2*complexconjugate(W13mat3x3))**2 + 8*abs(Y1QLL3x3*complexconjugate(W13mat3x1) - Y1LL3x3*complexconjugate(W13mat3x3))**2))/m3m13hat**3)/(16.*cmath.pi)',
                     texname = '\\text{W3m13hat}')

W3m43hat = Parameter(name = 'W3m43hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(m3m43hat*(8*(abs(W43mat2x1*Y1tQRR1x1)**2 + abs(W43mat2x1*Y1tQRR1x2)**2 + abs(W43mat2x1*Y1tQRR2x1)**2 + abs(W43mat2x1*Y1tQRR2x2)**2) + abs(W43mat2x1*Y1tRR1x1)**2 + abs(W43mat2x1*Y1tRR1x2)**2 + abs(W43mat2x1*Y1tRR1x3)**2 + abs(W43mat2x1*Y1tRR2x1)**2 + abs(W43mat2x1*Y1tRR2x2)**2 + abs(W43mat2x1*Y1tRR2x3)**2 + abs(W43mat2x1*Y1tRR3x1)**2 + abs(W43mat2x1*Y1tRR3x2)**2 + abs(W43mat2x1*Y1tRR3x3)**2 + 2*(abs(W43mat2x2*Y3LL1x1)**2 + abs(W43mat2x2*Y3LL1x2)**2 + abs(W43mat2x2*Y3LL1x3)**2 + abs(W43mat2x2*Y3LL2x1)**2 + abs(W43mat2x2*Y3LL2x2)**2 + abs(W43mat2x2*Y3LL2x3)**2 + abs(W43mat2x2*Y3LL3x1)**2 + abs(W43mat2x2*Y3LL3x2)**2 + abs(W43mat2x2*Y3LL3x3)**2) + 16*(abs(W43mat2x2*Y3QLL1x1)**2 + abs(W43mat2x2*Y3QLL1x2)**2 + abs(W43mat2x2*Y3QLL2x1)**2 + abs(W43mat2x2*Y3QLL2x2)**2)) + ((m3m43hat**2 - ymt**2)**2*(8*(abs(W43mat2x1*Y1tQRR1x3)**2 + abs(W43mat2x1*Y1tQRR2x3)**2) + 8*(abs(W43mat2x1*Y1tQRR3x1)**2 + abs(W43mat2x1*Y1tQRR3x2)**2) + 16*(abs(W43mat2x2*Y3QLL1x3)**2 + abs(W43mat2x2*Y3QLL2x3)**2) + 16*(abs(W43mat2x2*Y3QLL3x1)**2 + abs(W43mat2x2*Y3QLL3x2)**2)))/m3m43hat**3 + ((m3m43hat**2 - 4*ymt**2)**2*(8*abs(W43mat2x1*Y1tQRR3x3)**2 + 16*abs(W43mat2x2*Y3QLL3x3)**2))/m3m43hat**3)/(16.*cmath.pi)',
                     texname = '\\text{W3m43hat}')

W3p23hat = Parameter(name = 'W3p23hat',
                     nature = 'internal',
                     type = 'real',
                     value = '(((m3p23hat**2 - ymt**2)**2*(abs(W23mat3x1*Y2RL3x1)**2 + abs(W23mat3x1*Y2RL3x2)**2 + abs(W23mat3x1*Y2RL3x3)**2 + 2*abs(W23mat3x3*Y3LL3x1)**2 + 2*abs(W23mat3x3*Y3LL3x2)**2 + 2*abs(W23mat3x3*Y3LL3x3)**2))/m3p23hat**3 + m3p23hat*(abs(W23mat3x1*Y2LR1x1)**2 + abs(W23mat3x1*Y2LR1x2)**2 + abs(W23mat3x1*Y2LR1x3)**2 + abs(W23mat3x1*Y2LR2x1)**2 + abs(W23mat3x1*Y2LR2x2)**2 + abs(W23mat3x1*Y2LR2x3)**2 + abs(W23mat3x1*Y2LR3x1)**2 + abs(W23mat3x1*Y2LR3x2)**2 + abs(W23mat3x1*Y2LR3x3)**2 + abs(W23mat3x1*Y2RL1x1)**2 + abs(W23mat3x1*Y2RL1x2)**2 + abs(W23mat3x1*Y2RL1x3)**2 + abs(W23mat3x1*Y2RL2x1)**2 + abs(W23mat3x1*Y2RL2x2)**2 + abs(W23mat3x1*Y2RL2x3)**2 + abs(W23mat3x2*Y2tRL1x1)**2 + abs(W23mat3x2*Y2tRL1x2)**2 + abs(W23mat3x2*Y2tRL1x3)**2 + abs(W23mat3x2*Y2tRL2x1)**2 + abs(W23mat3x2*Y2tRL2x2)**2 + abs(W23mat3x2*Y2tRL2x3)**2 + abs(W23mat3x2*Y2tRL3x1)**2 + abs(W23mat3x2*Y2tRL3x2)**2 + abs(W23mat3x2*Y2tRL3x3)**2 + 2*abs(W23mat3x3*Y3LL1x1)**2 + 2*abs(W23mat3x3*Y3LL1x2)**2 + 2*abs(W23mat3x3*Y3LL1x3)**2 + 2*abs(W23mat3x3*Y3LL2x1)**2 + 2*abs(W23mat3x3*Y3LL2x2)**2 + 2*abs(W23mat3x3*Y3LL2x3)**2 + 48*abs(W23mat3x3*Y3QLL1x1)**2 + 48*abs(W23mat3x3*Y3QLL2x2)**2 + 48*abs(W23mat3x3*Y3QLL3x3)**2))/(16.*cmath.pi)',
                     texname = '\\text{W3p23hat}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I10a11 = Parameter(name = 'I10a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x1)',
                   texname = '\\text{I10a11}')

I10a12 = Parameter(name = 'I10a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x1)',
                   texname = '\\text{I10a12}')

I10a13 = Parameter(name = 'I10a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x1)',
                   texname = '\\text{I10a13}')

I10a21 = Parameter(name = 'I10a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x2)',
                   texname = '\\text{I10a21}')

I10a22 = Parameter(name = 'I10a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x2)',
                   texname = '\\text{I10a22}')

I10a23 = Parameter(name = 'I10a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x2)',
                   texname = '\\text{I10a23}')

I10a31 = Parameter(name = 'I10a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x3)',
                   texname = '\\text{I10a31}')

I10a32 = Parameter(name = 'I10a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x3)',
                   texname = '\\text{I10a32}')

I10a33 = Parameter(name = 'I10a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x3)',
                   texname = '\\text{I10a33}')

I11a11 = Parameter(name = 'I11a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR1x1)',
                   texname = '\\text{I11a11}')

I11a12 = Parameter(name = 'I11a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR2x1)',
                   texname = '\\text{I11a12}')

I11a13 = Parameter(name = 'I11a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR3x1)',
                   texname = '\\text{I11a13}')

I11a21 = Parameter(name = 'I11a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR1x2)',
                   texname = '\\text{I11a21}')

I11a22 = Parameter(name = 'I11a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR2x2)',
                   texname = '\\text{I11a22}')

I11a23 = Parameter(name = 'I11a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR3x2)',
                   texname = '\\text{I11a23}')

I11a31 = Parameter(name = 'I11a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR1x3)',
                   texname = '\\text{I11a31}')

I11a32 = Parameter(name = 'I11a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR2x3)',
                   texname = '\\text{I11a32}')

I11a33 = Parameter(name = 'I11a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y2LR3x3)',
                   texname = '\\text{I11a33}')

I12a11 = Parameter(name = 'I12a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x1)',
                   texname = '\\text{I12a11}')

I12a12 = Parameter(name = 'I12a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x1)',
                   texname = '\\text{I12a12}')

I12a13 = Parameter(name = 'I12a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x1)',
                   texname = '\\text{I12a13}')

I12a21 = Parameter(name = 'I12a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x2)',
                   texname = '\\text{I12a21}')

I12a22 = Parameter(name = 'I12a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x2)',
                   texname = '\\text{I12a22}')

I12a23 = Parameter(name = 'I12a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x2)',
                   texname = '\\text{I12a23}')

I12a31 = Parameter(name = 'I12a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x3)',
                   texname = '\\text{I12a31}')

I12a32 = Parameter(name = 'I12a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x3)',
                   texname = '\\text{I12a32}')

I12a33 = Parameter(name = 'I12a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x3)',
                   texname = '\\text{I12a33}')

I13a11 = Parameter(name = 'I13a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x1)',
                   texname = '\\text{I13a11}')

I13a12 = Parameter(name = 'I13a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x2)',
                   texname = '\\text{I13a12}')

I13a13 = Parameter(name = 'I13a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x3)',
                   texname = '\\text{I13a13}')

I13a21 = Parameter(name = 'I13a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x1)',
                   texname = '\\text{I13a21}')

I13a22 = Parameter(name = 'I13a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x2)',
                   texname = '\\text{I13a22}')

I13a23 = Parameter(name = 'I13a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x3)',
                   texname = '\\text{I13a23}')

I13a31 = Parameter(name = 'I13a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x1)',
                   texname = '\\text{I13a31}')

I13a32 = Parameter(name = 'I13a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x2)',
                   texname = '\\text{I13a32}')

I13a33 = Parameter(name = 'I13a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x3)',
                   texname = '\\text{I13a33}')

I14a11 = Parameter(name = 'I14a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x1)',
                   texname = '\\text{I14a11}')

I14a12 = Parameter(name = 'I14a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x2)',
                   texname = '\\text{I14a12}')

I14a13 = Parameter(name = 'I14a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x3)',
                   texname = '\\text{I14a13}')

I14a21 = Parameter(name = 'I14a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x1)',
                   texname = '\\text{I14a21}')

I14a22 = Parameter(name = 'I14a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x2)',
                   texname = '\\text{I14a22}')

I14a23 = Parameter(name = 'I14a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x3)',
                   texname = '\\text{I14a23}')

I14a31 = Parameter(name = 'I14a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x1)',
                   texname = '\\text{I14a31}')

I14a32 = Parameter(name = 'I14a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x2)',
                   texname = '\\text{I14a32}')

I14a33 = Parameter(name = 'I14a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x3)',
                   texname = '\\text{I14a33}')

I15a11 = Parameter(name = 'I15a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x1)',
                   texname = '\\text{I15a11}')

I15a12 = Parameter(name = 'I15a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x1)',
                   texname = '\\text{I15a12}')

I15a13 = Parameter(name = 'I15a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x1)',
                   texname = '\\text{I15a13}')

I15a21 = Parameter(name = 'I15a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x2)',
                   texname = '\\text{I15a21}')

I15a22 = Parameter(name = 'I15a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x2)',
                   texname = '\\text{I15a22}')

I15a23 = Parameter(name = 'I15a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x2)',
                   texname = '\\text{I15a23}')

I15a31 = Parameter(name = 'I15a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL1x3)',
                   texname = '\\text{I15a31}')

I15a32 = Parameter(name = 'I15a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL2x3)',
                   texname = '\\text{I15a32}')

I15a33 = Parameter(name = 'I15a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(Y3QLL3x3)',
                   texname = '\\text{I15a33}')

I16a11 = Parameter(name = 'I16a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL1x1',
                   texname = '\\text{I16a11}')

I16a12 = Parameter(name = 'I16a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL1x2',
                   texname = '\\text{I16a12}')

I16a13 = Parameter(name = 'I16a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL1x3',
                   texname = '\\text{I16a13}')

I16a21 = Parameter(name = 'I16a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL2x1',
                   texname = '\\text{I16a21}')

I16a22 = Parameter(name = 'I16a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL2x2',
                   texname = '\\text{I16a22}')

I16a23 = Parameter(name = 'I16a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL2x3',
                   texname = '\\text{I16a23}')

I16a31 = Parameter(name = 'I16a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL3x1',
                   texname = '\\text{I16a31}')

I16a32 = Parameter(name = 'I16a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL3x2',
                   texname = '\\text{I16a32}')

I16a33 = Parameter(name = 'I16a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1LL3x3',
                   texname = '\\text{I16a33}')

I17a11 = Parameter(name = 'I17a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL1x1',
                   texname = '\\text{I17a11}')

I17a12 = Parameter(name = 'I17a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL1x2',
                   texname = '\\text{I17a12}')

I17a13 = Parameter(name = 'I17a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL1x3',
                   texname = '\\text{I17a13}')

I17a21 = Parameter(name = 'I17a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL2x1',
                   texname = '\\text{I17a21}')

I17a22 = Parameter(name = 'I17a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL2x2',
                   texname = '\\text{I17a22}')

I17a23 = Parameter(name = 'I17a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL2x3',
                   texname = '\\text{I17a23}')

I17a31 = Parameter(name = 'I17a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL3x1',
                   texname = '\\text{I17a31}')

I17a32 = Parameter(name = 'I17a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL3x2',
                   texname = '\\text{I17a32}')

I17a33 = Parameter(name = 'I17a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3LL3x3',
                   texname = '\\text{I17a33}')

I18a11 = Parameter(name = 'I18a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x1',
                   texname = '\\text{I18a11}')

I18a12 = Parameter(name = 'I18a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x2',
                   texname = '\\text{I18a12}')

I18a13 = Parameter(name = 'I18a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x3',
                   texname = '\\text{I18a13}')

I18a21 = Parameter(name = 'I18a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x1',
                   texname = '\\text{I18a21}')

I18a22 = Parameter(name = 'I18a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x2',
                   texname = '\\text{I18a22}')

I18a23 = Parameter(name = 'I18a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x3',
                   texname = '\\text{I18a23}')

I18a31 = Parameter(name = 'I18a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x1',
                   texname = '\\text{I18a31}')

I18a32 = Parameter(name = 'I18a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x2',
                   texname = '\\text{I18a32}')

I18a33 = Parameter(name = 'I18a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x3',
                   texname = '\\text{I18a33}')

I19a11 = Parameter(name = 'I19a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x1',
                   texname = '\\text{I19a11}')

I19a12 = Parameter(name = 'I19a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x1',
                   texname = '\\text{I19a12}')

I19a13 = Parameter(name = 'I19a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x1',
                   texname = '\\text{I19a13}')

I19a21 = Parameter(name = 'I19a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x2',
                   texname = '\\text{I19a21}')

I19a22 = Parameter(name = 'I19a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x2',
                   texname = '\\text{I19a22}')

I19a23 = Parameter(name = 'I19a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x2',
                   texname = '\\text{I19a23}')

I19a31 = Parameter(name = 'I19a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL1x3',
                   texname = '\\text{I19a31}')

I19a32 = Parameter(name = 'I19a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL2x3',
                   texname = '\\text{I19a32}')

I19a33 = Parameter(name = 'I19a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y1QLL3x3',
                   texname = '\\text{I19a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I20a11 = Parameter(name = 'I20a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I20a11}')

I20a12 = Parameter(name = 'I20a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I20a12}')

I20a13 = Parameter(name = 'I20a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I20a13}')

I20a21 = Parameter(name = 'I20a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I20a21}')

I20a22 = Parameter(name = 'I20a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I20a22}')

I20a23 = Parameter(name = 'I20a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I20a23}')

I20a31 = Parameter(name = 'I20a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I20a31}')

I20a32 = Parameter(name = 'I20a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I20a32}')

I20a33 = Parameter(name = 'I20a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I20a33}')

I21a11 = Parameter(name = 'I21a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I21a11}')

I21a12 = Parameter(name = 'I21a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I21a12}')

I21a13 = Parameter(name = 'I21a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I21a13}')

I21a21 = Parameter(name = 'I21a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I21a21}')

I21a22 = Parameter(name = 'I21a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I21a22}')

I21a23 = Parameter(name = 'I21a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I21a23}')

I21a31 = Parameter(name = 'I21a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I21a31}')

I21a32 = Parameter(name = 'I21a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I21a32}')

I21a33 = Parameter(name = 'I21a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I21a33}')

I22a11 = Parameter(name = 'I22a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR1x1',
                   texname = '\\text{I22a11}')

I22a12 = Parameter(name = 'I22a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR1x2',
                   texname = '\\text{I22a12}')

I22a13 = Parameter(name = 'I22a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR1x3',
                   texname = '\\text{I22a13}')

I22a21 = Parameter(name = 'I22a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR2x1',
                   texname = '\\text{I22a21}')

I22a22 = Parameter(name = 'I22a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR2x2',
                   texname = '\\text{I22a22}')

I22a23 = Parameter(name = 'I22a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR2x3',
                   texname = '\\text{I22a23}')

I22a31 = Parameter(name = 'I22a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR3x1',
                   texname = '\\text{I22a31}')

I22a32 = Parameter(name = 'I22a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR3x2',
                   texname = '\\text{I22a32}')

I22a33 = Parameter(name = 'I22a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y2LR3x3',
                   texname = '\\text{I22a33}')

I23a11 = Parameter(name = 'I23a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I23a11}')

I23a12 = Parameter(name = 'I23a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I23a12}')

I23a13 = Parameter(name = 'I23a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I23a13}')

I23a21 = Parameter(name = 'I23a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I23a21}')

I23a22 = Parameter(name = 'I23a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I23a22}')

I23a23 = Parameter(name = 'I23a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I23a23}')

I23a31 = Parameter(name = 'I23a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I23a31}')

I23a32 = Parameter(name = 'I23a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I23a32}')

I23a33 = Parameter(name = 'I23a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I23a33}')

I24a11 = Parameter(name = 'I24a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I24a11}')

I24a12 = Parameter(name = 'I24a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I24a12}')

I24a13 = Parameter(name = 'I24a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I24a13}')

I24a21 = Parameter(name = 'I24a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I24a21}')

I24a22 = Parameter(name = 'I24a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I24a22}')

I24a23 = Parameter(name = 'I24a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I24a23}')

I24a31 = Parameter(name = 'I24a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I24a31}')

I24a32 = Parameter(name = 'I24a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I24a32}')

I24a33 = Parameter(name = 'I24a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I24a33}')

I25a11 = Parameter(name = 'I25a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I25a11}')

I25a12 = Parameter(name = 'I25a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I25a12}')

I25a13 = Parameter(name = 'I25a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I25a13}')

I25a21 = Parameter(name = 'I25a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I25a21}')

I25a22 = Parameter(name = 'I25a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I25a22}')

I25a23 = Parameter(name = 'I25a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I25a23}')

I25a31 = Parameter(name = 'I25a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I25a31}')

I25a32 = Parameter(name = 'I25a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I25a32}')

I25a33 = Parameter(name = 'I25a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I25a33}')

I26a11 = Parameter(name = 'I26a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x1',
                   texname = '\\text{I26a11}')

I26a12 = Parameter(name = 'I26a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x1',
                   texname = '\\text{I26a12}')

I26a13 = Parameter(name = 'I26a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x1',
                   texname = '\\text{I26a13}')

I26a21 = Parameter(name = 'I26a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x2',
                   texname = '\\text{I26a21}')

I26a22 = Parameter(name = 'I26a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x2',
                   texname = '\\text{I26a22}')

I26a23 = Parameter(name = 'I26a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x2',
                   texname = '\\text{I26a23}')

I26a31 = Parameter(name = 'I26a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL1x3',
                   texname = '\\text{I26a31}')

I26a32 = Parameter(name = 'I26a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL2x3',
                   texname = '\\text{I26a32}')

I26a33 = Parameter(name = 'I26a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Y3QLL3x3',
                   texname = '\\text{I26a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

I5a11 = Parameter(name = 'I5a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL1x1)',
                  texname = '\\text{I5a11}')

I5a12 = Parameter(name = 'I5a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL1x2)',
                  texname = '\\text{I5a12}')

I5a13 = Parameter(name = 'I5a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL1x3)',
                  texname = '\\text{I5a13}')

I5a21 = Parameter(name = 'I5a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL2x1)',
                  texname = '\\text{I5a21}')

I5a22 = Parameter(name = 'I5a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL2x2)',
                  texname = '\\text{I5a22}')

I5a23 = Parameter(name = 'I5a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL2x3)',
                  texname = '\\text{I5a23}')

I5a31 = Parameter(name = 'I5a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL3x1)',
                  texname = '\\text{I5a31}')

I5a32 = Parameter(name = 'I5a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL3x2)',
                  texname = '\\text{I5a32}')

I5a33 = Parameter(name = 'I5a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1LL3x3)',
                  texname = '\\text{I5a33}')

I6a11 = Parameter(name = 'I6a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL1x1)',
                  texname = '\\text{I6a11}')

I6a12 = Parameter(name = 'I6a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL1x2)',
                  texname = '\\text{I6a12}')

I6a13 = Parameter(name = 'I6a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL1x3)',
                  texname = '\\text{I6a13}')

I6a21 = Parameter(name = 'I6a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL2x1)',
                  texname = '\\text{I6a21}')

I6a22 = Parameter(name = 'I6a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL2x2)',
                  texname = '\\text{I6a22}')

I6a23 = Parameter(name = 'I6a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL2x3)',
                  texname = '\\text{I6a23}')

I6a31 = Parameter(name = 'I6a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL3x1)',
                  texname = '\\text{I6a31}')

I6a32 = Parameter(name = 'I6a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL3x2)',
                  texname = '\\text{I6a32}')

I6a33 = Parameter(name = 'I6a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3LL3x3)',
                  texname = '\\text{I6a33}')

I7a11 = Parameter(name = 'I7a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x1)',
                  texname = '\\text{I7a11}')

I7a12 = Parameter(name = 'I7a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x2)',
                  texname = '\\text{I7a12}')

I7a13 = Parameter(name = 'I7a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x3)',
                  texname = '\\text{I7a13}')

I7a21 = Parameter(name = 'I7a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x1)',
                  texname = '\\text{I7a21}')

I7a22 = Parameter(name = 'I7a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x2)',
                  texname = '\\text{I7a22}')

I7a23 = Parameter(name = 'I7a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x3)',
                  texname = '\\text{I7a23}')

I7a31 = Parameter(name = 'I7a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x1)',
                  texname = '\\text{I7a31}')

I7a32 = Parameter(name = 'I7a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x2)',
                  texname = '\\text{I7a32}')

I7a33 = Parameter(name = 'I7a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x3)',
                  texname = '\\text{I7a33}')

I8a11 = Parameter(name = 'I8a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x1)',
                  texname = '\\text{I8a11}')

I8a12 = Parameter(name = 'I8a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x1)',
                  texname = '\\text{I8a12}')

I8a13 = Parameter(name = 'I8a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x1)',
                  texname = '\\text{I8a13}')

I8a21 = Parameter(name = 'I8a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x2)',
                  texname = '\\text{I8a21}')

I8a22 = Parameter(name = 'I8a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x2)',
                  texname = '\\text{I8a22}')

I8a23 = Parameter(name = 'I8a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x2)',
                  texname = '\\text{I8a23}')

I8a31 = Parameter(name = 'I8a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL1x3)',
                  texname = '\\text{I8a31}')

I8a32 = Parameter(name = 'I8a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL2x3)',
                  texname = '\\text{I8a32}')

I8a33 = Parameter(name = 'I8a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y1QLL3x3)',
                  texname = '\\text{I8a33}')

I9a11 = Parameter(name = 'I9a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL1x1)',
                  texname = '\\text{I9a11}')

I9a12 = Parameter(name = 'I9a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL1x2)',
                  texname = '\\text{I9a12}')

I9a13 = Parameter(name = 'I9a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL1x3)',
                  texname = '\\text{I9a13}')

I9a21 = Parameter(name = 'I9a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL2x1)',
                  texname = '\\text{I9a21}')

I9a22 = Parameter(name = 'I9a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL2x2)',
                  texname = '\\text{I9a22}')

I9a23 = Parameter(name = 'I9a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL2x3)',
                  texname = '\\text{I9a23}')

I9a31 = Parameter(name = 'I9a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL3x1)',
                  texname = '\\text{I9a31}')

I9a32 = Parameter(name = 'I9a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL3x2)',
                  texname = '\\text{I9a32}')

I9a33 = Parameter(name = 'I9a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'complexconjugate(Y3QLL3x3)',
                  texname = '\\text{I9a33}')

