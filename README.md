# Complete Set of Feynman Rules for Scalar Leptoquarks
### Luc Schnell, October 2021

This GitLab project contains model files to determine the **complete set of Feynman Rules** for the **five scalar leptoquark (LQ) fields $`\Phi_1, \Phi_{\tilde{1}}, \Phi_2, \Phi_{\tilde{2}},\Phi_3`$**. These include **LQ-LQ-Higgs(-Higgs)**, **LQ-LQ-LQ(-Higgs)** and **LQ-LQ-LQ-LQ** interactions, as well as couplings to the **Standard Model (SM) fermions** and **gauge bosons**. The model files are based on the Mathematica package **FeynRules 2.3.36** [[1](https://arxiv.org/abs/1310.1921)].

The Mathematica notebook [SLQrules.nb](SLQrules.nb) is used to derive the Feynman rules and output the files for UFO, FeynArts, etc. The FeynRules model files can be found in the folder [SLQrules](SLQrules).

You can find more information in our article [arXiv:2105.04844](https://arxiv.org/pdf/2105.04844.pdf) **[hep-ph]**. **Please cite [Crivellin:2021tmz](https://inspirehep.net/literature/1862806) when using any of the content provided in this GitLab directory.** 

### SLQrules.nb
The notebook [SLQrules.nb](SLQrules.nb) is used to display and export the Feynman rules. Having loaded the model files in [SLQrules](SLQrules), the different parts of the Lagrangian can be accessed via the FeynRules variable names

| Lagrangian: Latex name | Lagrangian: Code name | Description |
| ------ | ------ | ------ |
| $`\mathcal{L}^{\text{LQ}}`$ | `LQall` | Entire leptoquark Lagrangian. |
| $`\mathcal{L}_{2\Phi}`$ | `LQ2Phi` | LQ masses and LQ-LQ-Higgs(-Higgs) interactions. |
| $`\mathcal{L}_{kin}`$ | `LQkin` | Kinetic terms and couplings to gauge bosons. |
| $`\mathcal{L}_{f}`$ | `LQf` | Couplings to SM fermions. |
| $`\mathcal{L}_{3\Phi}`$ | `LQ3Phi` | LQ-LQ-LQ(-Higgs) interactions. |
| $`\mathcal{L}_{4\Phi}`$ | `LQ4Phi` | LQ-LQ-LQ-LQ interactions. |

The command 
```mathematica
FeynmanRules[LQall];
```
is used to derive the Feynman rules, here for the entire Lagrangian $`\mathcal{L}^{\text{LQ}}`$. The Feynman rules can be exported as UFO files to be used in MadGraph. This is done via the command
```mathematica
FeynmanGauge = False;
WriteUFO[LSM + LQall, Output -> "SLQrules-UFO"];
```
The boolean `FeynmanGauge` on the first line is used to switch between Feynman and unitary gauge. The files are exported to SLQrules/SLQrules-UFO in your FeynRules directory. Similarly, the export to FeynArts is carried out using 
```mathematica
FeynmanGauge = False;
WriteFeynArtsOutput[LSM + LQall, Output -> "SLQrules-FA"];
```
We provide prefabricated FeynArts files under [FeynArts/SLQrules-FA](FeynArts/SLQrules-FA). Additionally, the export to MadGraph with the counterterms needed for NLO computations as well as Sherpa, CalcHep or Whizard is possible. The corresponding commands can be found in [SLQrules.nb](SLQrules.nb), but we opt not to include prefabricated files for these applications. 
